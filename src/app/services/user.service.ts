import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    baseEndpoint = 'https://randomuser.me/api/';

    getUser() {
        return fetch(this.baseEndpoint);
    }
} 