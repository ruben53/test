

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Tour of Heroes';

  
  /*constructor(private userService: UserService) {
    
    this.userService.getUser()
      .then(r => r.json())
      .then(user => {
        
        console.log(user.results);
      })
  } */
}