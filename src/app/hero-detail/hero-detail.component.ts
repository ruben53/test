import { Component, OnInit, Input } from '@angular/core';
import { Hero} from '../hero';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {

  /*Usó de @Input para hacer que la propiedad del hero
   esté disponible para vincularla con el HeroesComponent externo.*/
  @Input() hero?: Hero;
  
  constructor() { }

  ngOnInit(): void {
  }

}
