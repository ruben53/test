import { Component,HostBinding, OnInit } from '@angular/core';
import { Person } from '../person';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  person: Person = {
    id: 1,
    name: 'Workoholics'
  };
  
  //test event binding
  disabled =true;
  show = false;
  showMore = false;
  showBlock(){
    this.show = !this.show
  }

  showMoreEvent(){
    this.showMore = !this.showMore
  }

  clickEvent(){
    this.disabled = !this.disabled
  }
  @HostBinding('class.dashboard') darkModeDashboard = false;
      setModeColor(){
        this.darkModeDashboard = !this.darkModeDashboard;
  }
}
