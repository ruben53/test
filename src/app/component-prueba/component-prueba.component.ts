import { Component, Directive, HostBinding,HostListener,Input, OnInit } from '@angular/core';
import { HeroService } from '../hero.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-component-prueba',
  templateUrl: './component-prueba.component.html',
  styleUrls: ['./component-prueba.component.css']
})


export class ComponentPruebaComponent implements OnInit {

      /*constructor() { }*/
      allData = [];
      birthday = new Date(1988, 3, 15)
      amount = 19.99
      // este metodo es llamado al inicializarse el componente
      ngOnInit(): void {

              const url = 'https://randomuser.me/api/';
              // obtengo datos utilizando fetch
              fetch(url)
              .then(response => response.json())
              .then(data => {
              //console.log(data.results[0].name.first)
              this.allData = data.results[0].name.first
              //console.log(this.allData)
                

              });
          
      }

  
      text = 'soy un componente dinamico';

      //desabilitando input 
      disabled = true;
      handleClick(){
        this.disabled = !this.disabled;
      }

      //cambiando estilos a css
      @HostBinding('class.dark') darkMode = false;
      setDarkMode(){
        this.darkMode = true;
      }

      setLightMode(){
        this.darkMode = false;
      }

  
  
}
