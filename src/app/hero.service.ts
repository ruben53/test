import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Hero} from './hero';
import { HEROES } from './mock-hero';


@Injectable({
  providedIn: 'root'
})
export class HeroService {

  getHeroes(): Hero[] {
    return HEROES;
  }

}
