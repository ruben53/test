import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HEROES } from '../mock-hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  //Add a hero property
  //hero = 'Windstorm';

  heroes =  HEROES;
  //Interface

  /*
  hero: Hero = {
    id: 1,
    name: 'Windstorm angular'
  };*/


  //add event click
selectedHero?: Hero;


  constructor() { }

  ngOnInit(): void {
    
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

}
